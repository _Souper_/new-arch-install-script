#!/bin/bash

if [ ! -f partitions.py ]; 
then
    wget https://gitlab.com/_Souper_/new-arch-install-script/raw/master/partitions.py
    clear
fi

[ -d /sys/firmware/efi ] && bootmode="UEFI" || bootmode="BIOS"

echo Booted in $bootmode
echo

echo Note, it is automatically assumed that you already have partitioned your disk. You can do that in cfdisk.
echo It is also automatically assumed, if you are on UEFI that you marked your UEFI partition as type EFI.
echo This tool will only format them and mount them and then install a base Arch install.
echo

read -p "Proceed with installation?: " -n 1 -r
echo
if ! [[ $REPLY =~ ^[Yy]$ ]]
then
    exit
fi

function getPartition()
{
    options=$(lsblk -lno size,name,model --json | python partitions.py | awk '{print $1, $2}')
    cmd=(dialog --stdout \
            --ok-label "Proceed" \
            --menu "$1" 22 76 16)
    partSelection=$("${cmd[@]}" ${options})

    if [[ $partSelection = "" ]]
    then
        echo No selection made, exiting.
        exit
    fi

    partSelection="/dev/"$partSelection
    clear
    read -p "You selected "$partSelection" Continue? " -n 1 -r
    echo
    if ! [[ $REPLY =~ ^[Yy]$ ]]
    then
        exit
    fi
}

getPartition "Select root(/) Partition"
installdisk=$(echo $partSelection | sed 's/[0-9]*//g')

mkfs.ext4 $partSelection

mount $partSelection /mnt/

if [[ $bootmode = "UEFI" ]]
then
    getPartition "Select UEFI(/boot) Partition"

    mkdir /mnt/boot/
    read -p "!!! WARNING !!! Do you want to format UEFI? !!! WARNING !!! " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        mkfs.vfat -F32 $partSelection
    fi
    mount $partSelection /mnt/boot/
fi

read -p "Do you want a separate /home partition? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    getPartition "Select home(/home) Partition"
    mkdir /mnt/home/
    mkfs.ext4 $partSelection
    mount $partSelection /mnt/home/
fi
clear

echo "Additional packages to install? Separated by space, of course. (Your DE/Graphics card drivers/Web browser): "
read addstuff
pacstrap /mnt base linux linux-firmware nano vim base-devel $addstuff ntp efibootmgr gnome-keyring grub sddm ttf-dejavu git wget curl sudo dhcpcd wpa_supplicant dialog networkmanager network-manager-applet xterm
echo Pacstrapped, running post-install script.
wget https://gitlab.com/_Souper_/arch-install-script/raw/master/installphase2.sh -O /mnt/root/installphase2.sh
chmod +x /mnt/root/installphase2.sh
arch-chroot /mnt bash /root/installphase2.sh $bootmode $installdisk

sync

read -p "Installation finished. Unmount and reboot? " -n 1 -r
echo
if ! [[ $REPLY =~ ^[Yy]$ ]]
then
    genfstab -U /mnt >> /mnt/etc/fstab
    echo Got a negative response. Aborting.
    exit
fi

rm /mnt/root/installphase2.sh
genfstab -U /mnt >> /mnt/etc/fstab

sync
umount -R /mnt
reboot
