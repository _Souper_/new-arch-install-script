import sys, json, re;

ye = json.load(sys.stdin);

model = "";

for device in ye['blockdevices']:
    if device['name'].startswith('sr'):
        continue;
    if 'model' in device:
        tempModel = device['model'];
        if not tempModel == None:
            model = device['model'];
            model = model.replace("_", "-");
    m = re.search(r'\d+$', device['name']);
    if m is None:
        continue;
    print(device['name'] + " " + device['size'] + "_" + model);